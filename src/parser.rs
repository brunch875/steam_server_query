mod errors;

pub use errors::ParseError;

use std::io::{Cursor, BufRead, Read};

/// Marks implementor as being capable of parsing `T`
pub trait Parser<T> {
    /// The type returned on a parsing error
    type Error;
    /// Performs the parsing
    fn parse(&mut self) -> Result<T, Self::Error>;
}

/// Acts as a cursor over a raw steam server query response
///
/// Implements parsing over the buffer any encodable [data type] defined in a server query response
///
/// [data type]: https://developer.valvesoftware.com/wiki/Server_queries#Data_Types
pub struct SteamDataParser<'a> {
    cursor: Cursor<&'a [u8]>,
}

impl<'a> SteamDataParser<'a> {
    pub fn new(buffer: &'a [u8]) -> Self {
        SteamDataParser { cursor: Cursor::new(buffer) }
    }
}

impl Parser<u8> for SteamDataParser<'_> {
    type Error = ParseError;
    fn parse(&mut self) -> Result<u8, Self::Error> {
        let mut buffer = [0u8];
        self.cursor.read_exact(&mut buffer)?;
        Ok(buffer[0])
    }
}

impl Parser<u16> for SteamDataParser<'_> {
    type Error = ParseError;
    fn parse(&mut self) -> Result<u16, Self::Error> {
        let mut buffer = [0u8; 2];
        self.cursor.read_exact(&mut buffer)?;
        Ok(u16::from_le_bytes(buffer))
    }
}

impl<'a> Parser<String> for SteamDataParser<'a> {
    type Error = ParseError;
    fn parse(&mut self) -> Result<String, Self::Error> {
        let mut vector = Vec::new();
        self.cursor.read_until(0u8, &mut vector)?;

        if let Some(0u8) = vector.pop() {
            Ok(String::from_utf8(vector)?)
        } else {
            Err(ParseError::UnterminatedString)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn normal_operation() -> Result<(), ParseError> {
        let buffer = [ 2u8, b'h', b'i', 0u8, 0u8, 133u8 ];
        let mut parser = SteamDataParser::new(&buffer);

        assert_eq!(2u8, parser.parse()?);
        assert_eq!("hi", Parser::<String>::parse(&mut parser)?);
        assert_eq!(0u8, parser.parse()?);
        assert_eq!(133u8, parser.parse()?);
        Ok(())
    }

    #[test]
    fn short_buffer() -> Result<(), ParseError> {
        let buffer = [2u8];
        let mut parser = SteamDataParser::new(&buffer);
        assert_eq!(2u8, parser.parse()?);

        match Parser::<String>::parse(&mut parser) {
            Err(ParseError::UnterminatedString) => (),
            something_else => panic!(something_else),
        };

        match Parser::<u8>::parse(&mut parser) {
            Err(ParseError::IoError(_)) => Ok(()),
            something_else => panic!(something_else),
        }

    }
}
