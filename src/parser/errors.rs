use std::io;
use std::{error::Error, fmt};
use std::string::FromUtf8Error;

#[derive(Debug)]
pub enum ParseError {
    IoError(io::Error),
    UnterminatedString,
    UTF8Error(FromUtf8Error)
}

impl Error for ParseError {}
impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error while parsing")
    }
}

impl From<io::Error> for ParseError {
    fn from(error: io::Error) -> Self {
        Self::IoError(error)
    }
}

impl From<FromUtf8Error> for ParseError {
    fn from(error: FromUtf8Error) -> Self {
        Self::UTF8Error(error)
    }
}

