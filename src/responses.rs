use std::convert::TryFrom;
use super::parser::{SteamDataParser, Parser};

/// General information on the server, such as players or map
///
/// Partial information extracted from [A2S_INFO](https://developer.valvesoftware.com/wiki/Server_queries#A2S_INFO)
#[derive(Debug)]
pub struct A2SInfo {
    /// Name of the current map of the server
    pub map: String,
    /// Current amount of players playing in the server
    pub current_players: u8,
    /// Maximum amount of players the server admits
    pub max_players: u8,
}

impl TryFrom<&[u8]> for A2SInfo {
    type Error=();
    fn try_from(data: &[u8]) -> Result<Self, Self::Error> {
        let mut parser = SteamDataParser::new(data);

        macro_rules! next_token {
            () => { parser.parse().map_err(|_| ())? }
        }

        let header: u8 = next_token!();
        if header != b'I' {
            return Err(());
        }

        let _protocol: u8 = next_token!();
        let _server_name: String = next_token!();
        let map: String = next_token!();
        let _folder: String = next_token!();
        let _game: String = next_token!();
        let _app_id: u16 = next_token!();
        let current_players: u8 = next_token!();
        let max_players: u8 = next_token!();

        Ok(A2SInfo { map, current_players, max_players })
    }
}
