#![warn(missing_docs)]

//! Library to query a steam server using [steam server queries]
//!
//! This library only implements fetching partially [A2S_INFO], containing the current map name and
//! players
//!
//! [steam server queries]: https://developer.valvesoftware.com/wiki/Server_queries
//! [A2S_INFO]: https://developer.valvesoftware.com/wiki/Server_queries#A2S_INFO

mod parser;
mod responses;
pub use responses::A2SInfo;

use std::io;
use std::net::{UdpSocket, ToSocketAddrs};
use std::convert::TryFrom;

const STEAM_MAX_PACKET_SIZE: usize = 1400;
const SINGLE_PACKET_HEADER: [u8; 4] = [0xFF; 4];
const A2S_INFO_REQUEST: [u8; 25] =  *b"\xFF\xFF\xFF\xFFTSource Engine Query\0";

/// Provides mechanisms to query a steam server for information
pub struct SteamQuerier<T: ToSocketAddrs + Copy> {
    socket: UdpSocket,
    target: T,
}


/// Error type when failing to get server information
#[derive(Debug)]
pub enum QueryError {
    /// There was a socket error
    Socket(std::io::Error),
    /// Server took longer than 5 seconds to reply so the packet is assumed lost
    Timeout,
    /// Server did respond but the response was malformed and not understood,
    MalformedResponse,
}

impl From<std::io::Error> for QueryError {
    fn from(error: std::io::Error) -> Self {
        QueryError::Socket(error)
    }
}

impl<T: ToSocketAddrs + Copy> SteamQuerier<T> {
    /// Creates a new steam querier to send requests to `target`
    ///
    /// Upon creation, the querier will bind itself to a random UDP socket for any interface from
    /// which to send messages. Note if `target` is invalid, this won't be checked at construction
    /// but it will fail when sending messages
    pub fn new(target: T) -> io::Result<SteamQuerier<T>> {
        let socket = UdpSocket::bind("0.0.0.0:0")?;
        socket.set_read_timeout(Some(std::time::Duration::from_secs(5))).unwrap();
        Ok(SteamQuerier { socket, target })
    }

    /// Gets current server information
    ///
    /// If the server takes longer than 5 seconds to respond or there was any network error,
    /// returns with error. This function will also fail if the `SteamQuerier` was created with an
    /// incorrect target.
    pub fn get_server_info(&self) -> Result<A2SInfo, QueryError> {
        self.socket.send_to(&A2S_INFO_REQUEST, self.target)?;
        let mut buf = [0u8; STEAM_MAX_PACKET_SIZE];
        let (bytes_read, _) = self.socket.recv_from(&mut buf)?;

        if bytes_read < 4 || buf[..4] != SINGLE_PACKET_HEADER {
            return Err(QueryError::MalformedResponse);
        }

        A2SInfo::try_from(&buf[4..]).map_err(|_| QueryError::MalformedResponse)
    }
}
